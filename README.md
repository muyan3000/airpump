#  CS1237气泵控制

#### 介绍
淘宝20元的充气泵主体，带的传感器是DSH700，数模模块是CS1237，通过esp32读取气压值进行充气控制

![输入图片说明](%E5%BE%AE%E4%BF%A1%E5%9B%BE%E7%89%87_20211104202929.jpg)
![输入图片说明](%E5%BE%AE%E4%BF%A1%E5%9B%BE%E7%89%87_20211104202935.jpg)
![输入图片说明](%E5%BE%AE%E4%BF%A1%E5%9B%BE%E7%89%87%E7%BC%96%E8%BE%91_20211104203005.jpg)

立创EDA开源硬件平台 https://oshwhub.com/muyan2020/cs1237-chong-qi-qi-beng!

![输入图片说明](%E5%B1%8F%E5%B9%95%E6%88%AA%E5%9B%BE%202022-01-07%20225915.png)
![输入图片说明](%E5%BE%AE%E4%BF%A1%E5%9B%BE%E7%89%87_20211228205845.jpg)
![输入图片说明](%E5%BE%AE%E4%BF%A1%E5%9B%BE%E7%89%87_20211228103117.jpg)

#### 软件架构
软件架构说明
![输入图片说明](QQ%E5%9B%BE%E7%89%8720220109154612.png)

#### 安装教程



#### 使用说明


#### 参与贡献
参考作者代码： https://post.smzdm.com/p/aoon4e6n/

