#include <Wire.h>
#include <WiFi.h>
#include <ESPmDNS.h>
#include <ArduinoOTA.h>
#include <WebServer.h>
#include <FS.h>
#include "SPIFFS.h"

#define LIGHT_NUMBER 3  //节点开关数量

#define LIGHT_1_PIN 23
#define LIGHT_2_PIN 22
#define LIGHT_3_PIN 21

//触摸指示led
#define TOUCHLED_1_PIN 27
#define TOUCHLED_2_PIN 26
#define TOUCHLED_3_PIN 25

//外部开关输入引脚
#define CONTROL_1_PIN 34
#define CONTROL_2_PIN 35
#define CONTROL_3_PIN 32

#define SDA_PIN 16
#define SCL_PIN 17

//#define INPUT_VOLTAGE_SENSE_PIN 33

double  R1_VOLTAGE = 68000; //68K
double  R2_VOLTAGE = 10000; // 10K

//一定要修改这里，使用自己的wifi信息
char mac_tmp[6];
const char *ssid = mac_tmp;  //Wifi名称
const char * password = "12345678";  //Wifi密码

const char* username = "admin";     //web用户名
const char* userpassword = "12345678"; //web用户密码

const char * ServerName = "ESP32-air-pump";


#include "esp32_air_pump_main.h"
